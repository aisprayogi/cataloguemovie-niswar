package com.example.niswar.favoritemovie.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.niswar.favoritemovie.R;
import com.example.niswar.favoritemovie.entity.MovieItems;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ListRecyclerMovieAdapter extends RecyclerView.Adapter<ListRecyclerMovieAdapter.MovieViewHolder> {
        private Context context;
        private ArrayList<MovieItems> movieData = new ArrayList<MovieItems>();

        public ListRecyclerMovieAdapter(Context context) {
            this.context = context;
        }

        public ArrayList<MovieItems> getMovieData() {
            return movieData;
        }

        public void setMovieData(ArrayList<MovieItems> movieData) {
            this.movieData = movieData;
            notifyDataSetChanged();
        }

        public void setMovieDataCursor(Cursor movieDataCursor) {
            ArrayList<MovieItems> movies = new ArrayList<MovieItems>();
            if(movieDataCursor!=null) {
                if (movieDataCursor.moveToFirst()) {
                    do {
                        movies.add(new MovieItems(movieDataCursor));
                    } while (movieDataCursor.moveToNext());
                }
                this.movieData = movies;
            }
            notifyDataSetChanged();
        }

        @Override
        public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            //Layout perbaris item recycle view
            View itemRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_row_items, parent, false);
            return new MovieViewHolder(itemRow);

        }

        @Override
        public void onBindViewHolder(MovieViewHolder holder, int position) {
            MovieItems mvItems = getMovieData().get(position);
            holder.bind(mvItems);
        }

        @Override
        public int getItemCount() {
            return getMovieData().size();
        }

        public class MovieViewHolder extends RecyclerView.ViewHolder {
            TextView tvTitle, tvDescription, tvDate;
            ImageView imgPoster;

            public MovieViewHolder(View view) {
                super(view);
                tvTitle = (TextView)view.findViewById(R.id.tv_item_title);
                tvDescription = (TextView) view.findViewById(R.id.tv_item_description);
                tvDate = (TextView) view.findViewById(R.id.tv_item_release_date);
                imgPoster = (ImageView) view.findViewById(R.id.poster);


            }

            public void bind(final MovieItems items){
                tvTitle.setText(items.getMovieTitle());
                tvDescription.setText(items.getMovieDescription());
                tvDate.setText(items.getMovieReleaseDate());

                Picasso.with(context).load("http://image.tmdb.org/t/p/w154/" + items.getMovieImage()).into(imgPoster);
                /*
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent movieDetailIntent = new Intent(context, MovieDetailActivity.class);
                        movieDetailIntent.putExtra(MovieDetailActivity.EXTRA_MOVIE_ITEMS, items);
                        context.startActivity(movieDetailIntent);

                    }
                });
                */

            }
        }




}
