package com.example.niswar.favoritemovie;

import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.example.niswar.favoritemovie.adapter.ListRecyclerMovieAdapter;

import static com.example.niswar.favoritemovie.db.DatabaseContract.CONTENT_URI;

public class FavoriteActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private ListRecyclerMovieAdapter adapter;
    ProgressBar progressBar;
    private RecyclerView recyclerView;

    private final int LOAD_MOVIE_ID = 110;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);


        getSupportActionBar().setTitle("Favorite Movies");

        recyclerView = findViewById(R.id.rv_movie_item);
        progressBar = findViewById(R.id.progress_bar);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ListRecyclerMovieAdapter(this);
        recyclerView.setAdapter(adapter);
        //lvMovie.setOnItemClickListener(this);

        recyclerView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        getSupportLoaderManager().initLoader(LOAD_MOVIE_ID, null,  this);
    }

    protected void onResume() {
        super.onResume();
        getSupportLoaderManager().restartLoader(LOAD_MOVIE_ID, null, this);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this, CONTENT_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.setMovieDataCursor(data);
        recyclerView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.setMovieDataCursor(null);
    }
}
