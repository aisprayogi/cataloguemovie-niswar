package com.example.niswar.cataloguemovie;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;

import static com.example.niswar.cataloguemovie.MainActivity.EXTRA_MOVIE_SEARCH;

public class SearchFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<MovieItems>> {

    //private static final String EXTRA_MOVIE_SEARCH = "extra_movie_search";


    private RecyclerView rvMovie;
    private ListRecyclerMovieAdapter adapter;
    String searchQuery;

    ProgressBar progressBar;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        searchQuery = getArguments().getString(EXTRA_MOVIE_SEARCH);
        Log.d("Testing", "Search Query onCreated()" +searchQuery);
        View view = inflater.inflate(R.layout.fragment, container, false);
        rvMovie = (RecyclerView)view.findViewById(R.id.rv_movie_item);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvMovie.setLayoutManager(layoutManager);

        LoaderManager loaderManager = getLoaderManager();
        Loader<ArrayList<MovieItems>> loader = loaderManager.getLoader(0);

        adapter = new ListRecyclerMovieAdapter(getContext());
        rvMovie.setAdapter(adapter);
        return view;

    }

    @Override
    public Loader<ArrayList<MovieItems>> onCreateLoader(int id, Bundle args) {

        return new MovieAsyncTaskLoader(getContext(), 3, searchQuery);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<MovieItems>> loader, ArrayList<MovieItems> data) {
        adapter.setMovieData(data);
        Log.d("Testting", data.toString());

        progressBar.setVisibility(View.GONE);


    }

    @Override
    public void onLoaderReset(Loader<ArrayList<MovieItems>> loader) {
        adapter.setMovieData(null);

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String searchQuery = getArguments().getString(EXTRA_MOVIE_SEARCH);
        Log.d("Testing", "Search Query onActivityCreated()" +searchQuery);
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_MOVIE_SEARCH, searchQuery);

        getLoaderManager().initLoader(0, bundle, this);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {


        super.onViewCreated(view, savedInstanceState);
        String searchQuery = getArguments().getString(EXTRA_MOVIE_SEARCH);
        Log.d("Testing", "Search Query" +searchQuery);

        progressBar = (ProgressBar)view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);



    }

}
