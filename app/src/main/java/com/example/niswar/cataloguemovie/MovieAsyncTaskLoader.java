package com.example.niswar.cataloguemovie;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentController;
import android.support.v4.app.FragmentHostCallback;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class MovieAsyncTaskLoader extends AsyncTaskLoader<ArrayList<MovieItems>>{
    private String movieTitle;
    private ArrayList<MovieItems> mItems;
    private boolean mvResult = false;

    private int loadMode; // 1: up coming, 2: now playing 3: Search

    //final FragmentController mFragments = FragmentController.createController(FragmentHostCallback);

    public MovieAsyncTaskLoader(Context context, int mode, String title ) {
        super(context);
        onContentChanged();
        this.movieTitle = title;
        this.loadMode = mode;
        Log.d("Testing", String.valueOf(this.loadMode));

    }

    @Override
    public ArrayList<MovieItems> loadInBackground() {
        SyncHttpClient client = new SyncHttpClient();

        String url ="";

        switch (loadMode){
            case 1: url="https://api.themoviedb.org/3/movie/now_playing?api_key=" +
                    BuildConfig.API_KEY+"&language=en-US=";
            break;
            case 2: url="https://api.themoviedb.org/3/movie/upcoming?api_key=" +
                    BuildConfig.API_KEY+"&language=en-US=";
            break;
            case 3: url = "https://api.themoviedb.org/3/search/movie?api_key=" +
                    BuildConfig.API_KEY+"&language=en-US&query="+movieTitle;
            break;


        }


        final ArrayList<MovieItems> movItems = new ArrayList<>(); //penampungan sementara

        Log.d("Testting", url);

        client.get(url, new AsyncHttpResponseHandler() {

            @Override
            public void onStart(){
                super.onStart();
                setUseSynchronousMode(true);
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String result = new String(responseBody);
                    JSONObject responseObj = new JSONObject(result);
                    JSONArray list = responseObj.getJSONArray("results");

                    Log.d("Testting", "onSuccess" + result);

                    for (int i = 0; i < list.length(); i++) {
                        JSONObject movie = list.getJSONObject(i);
                        MovieItems mvItems = new MovieItems(movie);
                        movItems.add(mvItems); //kalo sdh dapat data movItems (ArrayList)
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("Testting", error.getMessage());

            }
        });
        return movItems;
    }


    private void onReleaseResources(ArrayList<MovieItems> items) {
    }
    @Override
    protected void onStartLoading() {
        if (takeContentChanged())
            forceLoad();
        else if (mvResult)
            deliverResult(mItems);
    }

    public void deliverResult(ArrayList<MovieItems> items) {
        mItems = items;
        mvResult = true;
        super.deliverResult(items);
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
        if (mvResult) {
            onReleaseResources(mItems);
            mItems = null;
            mvResult = false;
        }
    }
}


