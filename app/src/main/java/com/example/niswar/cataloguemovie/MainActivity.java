package com.example.niswar.cataloguemovie;

import android.content.Intent;
import android.database.Cursor;
import android.location.SettingInjectorService;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.niswar.cataloguemovie.R.*;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    public static final String EXTRA_MOVIE_SEARCH = "EXTRA_MOVIE_SEARCH";
    ListView listView;
    MovieListAdapter adapter;

    SearchView searchView;

    private Cursor list;




    CircleImageView profileCircleImageView;
  //  String profileImageUrl = "https://media.licdn.com/mpr/mpr/shrinknp_400_400/AAEAAQAAAAAAAAb8AAAAJGVlMmE5ZmNiLTZlMDQtNDcyMi04OWUzLTcwYWIxZTMzYjhmZA.jpg";


    DrawerLayout drawer;
    Toolbar toolbar;
    ActionBarDrawerToggle toggle;

    ProgressBar progressBar;
    TextView tvProgressBar;

    private int progressStatus = 0;
    private Handler handler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_main);

        Toolbar toolbar = findViewById(id.toolbar);
        setSupportActionBar(toolbar);


//keluar masuk menu navigasi
        drawer = findViewById(id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

//Munculkan menu navigasi
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FragmentManager mFragmentManager = getSupportFragmentManager();
        FragmentTransaction mFragmentTrans = mFragmentManager.beginTransaction();
        UpcomingFragment upComingFragment = new UpcomingFragment();
        mFragmentTrans.replace(id.fragment_container, upComingFragment, UpcomingFragment.class.getSimpleName());

        mFragmentTrans.commit();



    }

//tekan tombol back, masukkan drawer
    public void onBackPressed(){
        if (drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }else {
            super.onBackPressed();
        }
    }


    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_cari, menu);
        MenuItem item = menu.findItem(id.menu_search);
        searchView = (SearchView)item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {  //pada saat search enter di klik
                Bundle bundle = new Bundle();
                bundle.putString(EXTRA_MOVIE_SEARCH, s);
                Log.d("Testting", "test" + s);


//load fragment setelah klik search
                FragmentManager mFragmentManager = getSupportFragmentManager();
                FragmentTransaction mFragmentTrans = mFragmentManager.beginTransaction();
                SearchFragment searchFragment = new SearchFragment();
                searchFragment.setArguments(bundle); //Kirim Query Search
                mFragmentTrans.replace(id.fragment_container, searchFragment, SearchFragment.class.getSimpleName());

                mFragmentTrans.commit();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);

    }




    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        drawer.closeDrawer(GravityCompat.START);

        FragmentManager mFragmentManager = getSupportFragmentManager();
        FragmentTransaction mFragmentTrans = mFragmentManager.beginTransaction();


        if (id == R.id.nav_search){

            Fragment fragment = mFragmentManager.findFragmentById(R.id.fragment_container);
            mFragmentTrans.hide(fragment);
            mFragmentTrans.commit();
            searchView.setIconified(false);

        }

        if (id == R.id.nav_now_playing) {
            Log.d("Debug:","Debag MainActivity NowPlaying");

            NowPlayingFragment nowPlayingFragment = new NowPlayingFragment();
            mFragmentTrans.replace(R.id.fragment_container, nowPlayingFragment, NowPlayingFragment.class.getSimpleName());

            mFragmentTrans.commit();
        }

        if (id == R.id.nav_upcoming) {
            UpcomingFragment upcomingFragment = new UpcomingFragment();
            mFragmentTrans.replace(R.id.fragment_container, upcomingFragment, UpcomingFragment.class.getSimpleName());

            mFragmentTrans.commit();
        }

        if (id == R.id.nav_favorite) {
            FavoriteFragment favoriteFragment = new FavoriteFragment();
            mFragmentTrans.replace(R.id.fragment_container, favoriteFragment, FavoriteFragment.class.getSimpleName());

            mFragmentTrans.commit();
            Log.d("Debug:","Debag MainActivity ke Favorite Fragment");

        }



        if (id == R.id.nav_language){

            Fragment fragment = mFragmentManager.findFragmentById(R.id.fragment_container);
            mFragmentTrans.hide(fragment);
            mFragmentTrans.commit();

            Intent intent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
            startActivity(intent);

        }


        return true;
    }
}
