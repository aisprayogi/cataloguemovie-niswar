package com.example.niswar.cataloguemovie;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class NowPlayingFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<MovieItems>>{


    private RecyclerView rvMovie;
    private ListRecyclerMovieAdapter adapter;

    ProgressBar progressBar;
    TextView tvProgressBar;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
           return inflater.inflate(R.layout.fragment, container, false);
    }

    @Override
    public Loader<ArrayList<MovieItems>> onCreateLoader(int i, Bundle bundle) {
        Log.d("Test", "Debug onCreateLoader() + No Playing");
        return new MovieAsyncTaskLoader(getContext(), 1, "");
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<MovieItems>> loader, ArrayList<MovieItems> data) {
        adapter.setMovieData(data);
        Log.d("Testting", data.toString());

        progressBar.setVisibility(View.GONE);  //Hilangkan Progress Bar

        //


    }

    @Override
    public void onLoaderReset(Loader<ArrayList<MovieItems>> loader) {
        adapter.setMovieData(null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        rvMovie = (RecyclerView)view.findViewById(R.id.rv_movie_item);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvMovie.setLayoutManager(layoutManager);

        LoaderManager loaderManager = getLoaderManager();
        Loader<ArrayList<MovieItems>> loader = loaderManager.getLoader(0);

        adapter = new ListRecyclerMovieAdapter(getContext());
        rvMovie.setAdapter(adapter);

        Bundle bundle = new Bundle();

        getLoaderManager().initLoader(0, bundle, this);

        Log.d("Test", "Debug onViewCreated()");

        progressBar = (ProgressBar)view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);   //Display Progress Bar



    }





}
