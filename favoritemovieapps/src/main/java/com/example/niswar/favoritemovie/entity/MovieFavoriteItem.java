package com.example.niswar.favoritemovie.entity;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.niswar.favoritemovie.db.DatabaseContract;

import static com.example.niswar.favoritemovie.db.DatabaseContract.getColumnInt;
import static com.example.niswar.favoritemovie.db.DatabaseContract.getColumnString;

public class MovieFavoriteItem implements Parcelable {

    private int id;
    private String title, description, date;


    protected MovieFavoriteItem(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.description = in.readString();
        this.date = in.readString();

    }

    public static final Creator<MovieFavoriteItem> CREATOR = new Creator<MovieFavoriteItem>() {
        @Override
        public MovieFavoriteItem createFromParcel(Parcel in) {
            return new MovieFavoriteItem(in);
        }

        @Override
        public MovieFavoriteItem[] newArray(int size) {
            return new MovieFavoriteItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.date);
    }

    public MovieFavoriteItem(){

    }

    public MovieFavoriteItem(Cursor cursor){
        this.id = getColumnInt(cursor, DatabaseContract.movieColumns._ID);
        this.title = getColumnString(cursor, DatabaseContract.movieColumns.TITLE);
        this.description = getColumnString(cursor, DatabaseContract.movieColumns.DESCRIPTION);
        this.date = getColumnString(cursor, DatabaseContract.movieColumns.RELEASE_DATE);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
