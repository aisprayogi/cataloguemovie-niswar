package com.example.niswar.cataloguemovie.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.niswar.cataloguemovie.db.DatabaseContract;

import static android.provider.BaseColumns._ID;
import static com.example.niswar.cataloguemovie.db.DatabaseContract.getColumnInt;
import static com.example.niswar.cataloguemovie.db.DatabaseContract.getColumnString;

public class MovieModel implements Parcelable {
    private String id;
    private String title;
    private String description;
    private String date;

    Cursor cursor;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public MovieModel(String id){
        this.id = id;
    }

    protected MovieModel(Parcel in) {
        this.id = in.readString();
        this.title = in.readString();
        this.description = in.readString();
        this.date = in.readString();
    }

    public static final Creator<MovieModel> CREATOR = new Creator<MovieModel>() {
        @Override
        public MovieModel createFromParcel(Parcel in) {
            return new MovieModel(in);
        }

        @Override
        public MovieModel[] newArray(int size) {
            return new MovieModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.date);


    }

    public MovieModel(){
        this.id = getColumnString(cursor, _ID);
        this.title = getColumnString(cursor, DatabaseContract.movieColumns.TITLE);
        this.description = getColumnString(cursor, DatabaseContract.movieColumns.DESCRIPTION);
        this.date = getColumnString(cursor, DatabaseContract.movieColumns.RELEASE_DATE);
    }
}
