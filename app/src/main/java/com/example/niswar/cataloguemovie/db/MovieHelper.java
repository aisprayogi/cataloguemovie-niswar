package com.example.niswar.cataloguemovie.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.provider.ContactsContract;
import android.util.Log;

import com.example.niswar.cataloguemovie.MovieItems;
import com.example.niswar.cataloguemovie.model.MovieModel;

import java.util.ArrayList;

import static android.provider.BaseColumns._ID;
import static com.example.niswar.cataloguemovie.db.DatabaseContract.movieColumns.DESCRIPTION;
import static com.example.niswar.cataloguemovie.db.DatabaseContract.movieColumns.MOVIE_ID;
import static com.example.niswar.cataloguemovie.db.DatabaseContract.movieColumns.RELEASE_DATE;
import static com.example.niswar.cataloguemovie.db.DatabaseContract.TABLE_MOVIE;
import static com.example.niswar.cataloguemovie.db.DatabaseContract.movieColumns.TITLE;

public class MovieHelper {

    private static String DATABASE_TABLE = DatabaseContract.TABLE_MOVIE;
    private Context context;
    private DatabaseHelper databaseHelper;

    private SQLiteDatabase sqliteDatabase;

    public MovieHelper(Context context) {
        this.context = context;
    }

    public MovieHelper open() throws SQLException {

        databaseHelper = new DatabaseHelper(context);
        sqliteDatabase = databaseHelper.getWritableDatabase();
        Log.d("DEBUGGING", "DataHelper open()");
        return this;

    }

    public void close() {
        sqliteDatabase.close();
    }

    public ArrayList<MovieModel> query(){
        ArrayList<MovieModel> arrayList = new ArrayList<MovieModel>();
        Cursor cursor = sqliteDatabase.query(DATABASE_TABLE
                ,null
                ,null
                ,null
                ,null
                ,null,_ID +" DESC"
                ,null);
        cursor.moveToFirst();
        MovieModel movieModel;
        if (cursor.getCount()>0) {
            do {

                movieModel = new MovieModel();
                movieModel.setId(cursor.getString(cursor.getColumnIndexOrThrow(MOVIE_ID)));
                movieModel.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(TITLE)));
                movieModel.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(DESCRIPTION)));
                movieModel.setDate(cursor.getString(cursor.getColumnIndexOrThrow(RELEASE_DATE)));

                arrayList.add(movieModel);
                cursor.moveToNext();

            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }







    public long insert(MovieModel movieModel){
        ContentValues initialValues =  new ContentValues();
        initialValues.put(MOVIE_ID, movieModel.getId());
        initialValues.put(TITLE, movieModel.getTitle());
        initialValues.put(DESCRIPTION, movieModel.getDescription());
        initialValues.put(RELEASE_DATE, movieModel.getDate());
        return sqliteDatabase.insert(DATABASE_TABLE, null, initialValues);
    }

    public int update(MovieModel movieModel){
        ContentValues args = new ContentValues();
        args.put(TITLE, movieModel.getTitle());
        args.put(DESCRIPTION, movieModel.getDescription());
        args.put(RELEASE_DATE, movieModel.getDate());
        return sqliteDatabase.update(DATABASE_TABLE, args, _ID + "= '" + movieModel.getId() + "'", null);
    }

    public int delete(int id){
        return sqliteDatabase.delete(TABLE_MOVIE, _ID + " = '"+id+"'", null);
    }



    public Cursor queryByIdProvider(String id) {
        return sqliteDatabase.query(DATABASE_TABLE, null
                , DatabaseContract.movieColumns.MOVIE_ID + " = ?"
                , new String[]{id}
                , null
                , null
                , null
                , null);
    }

    public Cursor queryProvider() {
        return sqliteDatabase.query(
                DATABASE_TABLE,
                null,
                null,
                null,
                null,
                null,
                _ID + " DESC"
        );
    }

    public long insertProvider(ContentValues values) {
        return sqliteDatabase.insert(DATABASE_TABLE, null, values);
    }

    public int updateProvider(String id, ContentValues values) {
        return sqliteDatabase.update(DATABASE_TABLE, values,
                DatabaseContract.movieColumns.ID + " = ?", new String[]{id});
    }

    public int deleteProvider(String id) {
        return sqliteDatabase.delete(DATABASE_TABLE,
                DatabaseContract.movieColumns.MOVIE_ID + " = ?", new String[]{id});
    }
}
