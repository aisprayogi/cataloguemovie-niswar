package com.example.niswar.favoritemovie.entity;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import static com.example.niswar.favoritemovie.db.DatabaseContract.*;
import static com.example.niswar.favoritemovie.db.DatabaseContract.movieColumns.*;

public class MovieItems implements Parcelable{

    private int id;
    private String movieTitle;
    private String movieDescription;
    private String movieImage;
    private String movieReleaseDate;

    public MovieItems(JSONObject obj) {
        try{
            int id = obj.getInt("id");
            String title = obj.getString("title");
            String description = obj.getString("overview");
            String image = obj.getString("poster_path");
            String date = obj.getString("release_date");

            this.id = id;
            this.movieTitle = title;
            this.movieDescription = description;
            this.movieImage = image;
            this.movieReleaseDate = date;

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public MovieItems(Cursor cursor) {
        try{
            int id = getColumnInt(cursor, MOVIE_ID);
            String title = getColumnString(cursor,TITLE);
            String description = getColumnString(cursor,DESCRIPTION);
            String image = getColumnString(cursor,POSTER_PATH);
            String date = getColumnString(cursor,RELEASE_DATE);

            this.id = id;
            this.movieTitle = title;
            this.movieDescription = description;
            this.movieImage = image;
            this.movieReleaseDate = date;

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String getMovieTitle() {
        return this.movieTitle;
    }

    public String getMovieDescription() {
        return movieDescription;
    }

    public String getMovieImage() {
        return movieImage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public void setMovieDescription(String movieDescription) {
        this.movieDescription = movieDescription;
    }

    public void setMovieImage(String movieImage) {
        this.movieImage = movieImage;
    }

    public String getMovieReleaseDate() {
        return movieReleaseDate;
    }

    public void setMovieReleaseDate(String movieReleaseDate) {
        this.movieReleaseDate = movieReleaseDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.movieTitle);
        dest.writeString(this.movieDescription);
        dest.writeString(this.movieImage);
        dest.writeString(this.movieReleaseDate);
    }

    protected MovieItems(Parcel in) {
        this.id = in.readInt();
        this.movieTitle = in.readString();
        this.movieDescription = in.readString();
        this.movieImage = in.readString();
        this.movieReleaseDate = in.readString();
    }

    public static final Creator<MovieItems> CREATOR = new Creator<MovieItems>() {
        @Override
        public MovieItems createFromParcel(Parcel source) {
            return new MovieItems(source);
        }

        @Override
        public MovieItems[] newArray(int size) {
            return new MovieItems[size];
        }
    };
}
