package com.example.niswar.cataloguemovie.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.niswar.cataloguemovie.db.DatabaseContract;
import com.example.niswar.cataloguemovie.db.MovieHelper;

import static com.example.niswar.cataloguemovie.db.DatabaseContract.AUTHORITY;
import static com.example.niswar.cataloguemovie.db.DatabaseContract.CONTENT_URI;

public class MovieProvider extends ContentProvider {

    private static final int MOV = 1;
    private static final int MOV_ID = 2;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {

        //content://com.example.niswar.cataloguemovie/movie
        sUriMatcher.addURI(AUTHORITY, DatabaseContract.TABLE_MOVIE, MOV);


        //content://com.example.niswar.cataloguemovie/movie/id
        sUriMatcher.addURI(AUTHORITY,
                DatabaseContract.TABLE_MOVIE+ "/#",
                MOV_ID);
    }

    private MovieHelper movieHelper;

    @Override
    public boolean onCreate() {
        movieHelper = new MovieHelper(getContext());
        movieHelper.open();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        Cursor cursor;
        switch(sUriMatcher.match(uri)){
            case MOV:
                Log.d("MovieDetails", "provider query1: "+uri);
                cursor = movieHelper.queryProvider();
                break;
            case MOV_ID:
                Log.d("MovieDetails", "provider query2: "+uri.getLastPathSegment());
                cursor = movieHelper.queryByIdProvider(uri.getLastPathSegment());
                break;
            default:
                cursor = null;
                break;
        }

        if (cursor != null){
            cursor.setNotificationUri(getContext().getContentResolver(),uri);
        }

        return cursor;

    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        long added ;

        switch (sUriMatcher.match(uri)){
            case MOV:
                added = movieHelper.insertProvider(contentValues);
                break;
            default:
                added = 0;
                break;
        }

        if (added > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return Uri.parse(CONTENT_URI + "/" + added);

    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        int deleted;
        Log.d("Provider", "delete: "+uri);
        switch (sUriMatcher.match(uri)) {
            case MOV_ID:
                Log.d("Provider1", "delete: "+uri);
                deleted =  movieHelper.deleteProvider(uri.getLastPathSegment());
                break;
            default:
                deleted = 0;
                break;
        }

        if (deleted > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return deleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        int updated ;
        switch (sUriMatcher.match(uri)) {
            case MOV_ID:
                updated =  movieHelper.updateProvider(uri.getLastPathSegment(),contentValues);
                break;
            default:
                updated = 0;
                break;
        }

        if (updated > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return updated;

    }
}
