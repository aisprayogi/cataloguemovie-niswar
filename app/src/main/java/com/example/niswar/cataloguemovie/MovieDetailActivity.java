package com.example.niswar.cataloguemovie;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.niswar.cataloguemovie.db.DatabaseContract;
import com.squareup.picasso.Picasso;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.example.niswar.cataloguemovie.db.DatabaseContract.movieColumns.DESCRIPTION;
import static com.example.niswar.cataloguemovie.db.DatabaseContract.movieColumns.ID;
import static com.example.niswar.cataloguemovie.db.DatabaseContract.movieColumns.MOVIE_ID;
import static com.example.niswar.cataloguemovie.db.DatabaseContract.movieColumns.POSTER_PATH;
import static com.example.niswar.cataloguemovie.db.DatabaseContract.movieColumns.RELEASE_DATE;
import static com.example.niswar.cataloguemovie.db.DatabaseContract.movieColumns.TITLE;

public class MovieDetailActivity extends AppCompatActivity{

    public static String EXTRA_MOVIE_ITEMS        = "extra_movie_items";


    private TextView tvTitle, tvOverview, tvReleaseDate;
    private ImageView imgPoster;
    private Context context;

    ToggleButton toggleButton;
    String id, title, description;

    public static int REQUEST_ADD = 100;
    public static int RESULT_ADD = 101;
    public static int REQUEST_UPDATE = 200;
    public static int RESULT_UPDATE = 201;
    public static int RESULT_DELETE = 301;

    Uri uri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);

        uri = getIntent().getData();

        tvTitle = (TextView)findViewById(R.id.detail_title);
        tvOverview = (TextView)findViewById(R.id.detail_description);
        tvReleaseDate = (TextView)findViewById(R.id.detail_release_date);
        imgPoster = (ImageView)findViewById(R.id.detail_poster);



        final MovieItems movieItems = getIntent().getParcelableExtra(EXTRA_MOVIE_ITEMS);

        SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = date_format.parse(movieItems.getMovieReleaseDate());

            SimpleDateFormat new_date_format = new SimpleDateFormat("EEEE, MMM dd, yyyy");
            String date_of_release = new_date_format.format(date);
            tvReleaseDate.setText(date_of_release);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        tvTitle.setText(movieItems.getMovieTitle());
        tvOverview.setText(movieItems.getMovieDescription());
        Picasso.with(context).load("http://image.tmdb.org/t/p/w500/"+ movieItems.getMovieImage()).into(imgPoster);

        toggleButton = (ToggleButton) findViewById(R.id.myToggleButton);
        if(isFavorite(movieItems.getId())) {
            toggleButton.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_favorite_black));
            toggleButton.setChecked(true);
        }
        else {
            toggleButton.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_favorite_border_black));
            toggleButton.setChecked(false);
        }
        //toggleButton.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_favorite_border_black));
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ContentValues values = new ContentValues();
                    values.put(MOVIE_ID, movieItems.getId());
                    values.put(TITLE, tvTitle.getText().toString());
                    values.put(DESCRIPTION, tvOverview.getText().toString());
                    values.put(RELEASE_DATE, tvReleaseDate.getText().toString());
                    values.put(POSTER_PATH, movieItems.getMovieImage());
                    uri = DatabaseContract.CONTENT_URI;
                    //uri = uri.buildUpon().appendPath(id).build();
                    Log.d("MovieDetail", "insert :" + uri);
                    getContentResolver().insert(DatabaseContract.CONTENT_URI, values);
                    //setResult(RESULT_ADD);
                    //finish();
                    //getContentResolver().delete(uri, null, null);
                    toggleButton.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_favorite_black));
                } else {
                    uri = DatabaseContract.CONTENT_URI;
                    uri = uri.buildUpon().appendPath(Integer.toString(movieItems.getId())).build();
                    Log.d("MovieDetail", "delete " + uri);
                    getContentResolver().delete(uri, null, null);
                    //getContentResolver().delete(uri, null, null);
                    toggleButton.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_favorite_border_black));
                }
            }
        });


    }

    private boolean isFavorite(int id) {
        uri = DatabaseContract.CONTENT_URI;
        uri = uri.buildUpon().appendPath(Integer.toString(id)).build();
        Log.d("MovieDetail", "isFavorite: "+uri);
        Cursor cursor = getContentResolver().query(uri,null,null,null,null);
        if(cursor != null){
            if(cursor.getCount()>0){
                return true;
            }
        }
        return false;

    }

}
