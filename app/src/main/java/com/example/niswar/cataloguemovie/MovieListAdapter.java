package com.example.niswar.cataloguemovie;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class MovieListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<MovieItems> items;

    private LayoutInflater layoutInflater;


    public MovieListAdapter(Context context){
        this.context = context;
        layoutInflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        if (items == null) {
            return 0;
        }
        return items.size(); //return total item dalam list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);  //return list item pada posisi khusus
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setItems(ArrayList<MovieItems> data){
        this.items = data;
        notifyDataSetChanged();   //perubahan listView

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.list_view_row_items, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        MovieItems currentItem = (MovieItems) getItem(position);
        viewHolder.tvTitle.setText(currentItem.getMovieTitle());
        viewHolder.tvDescription.setText(currentItem.getMovieDescription());
        viewHolder.tvDate.setText(currentItem.getMovieReleaseDate());

        Picasso.with(context).load("http://image.tmdb.org/t/p/w154/" + currentItem.getMovieImage()).into(viewHolder.imgPoster);

        return convertView;


    }

    private class ViewHolder{
        TextView tvTitle, tvDescription, tvDate;
        ImageView imgPoster;

        public ViewHolder(View view) {
            tvTitle = (TextView)view.findViewById(R.id.tv_item_title);
            tvDescription = (TextView) view.findViewById(R.id.tv_item_description);
            tvDate = (TextView) view.findViewById(R.id.tv_item_release_date);
            imgPoster = (ImageView) view.findViewById(R.id.poster);
        }

    }


}
