package com.example.niswar.cataloguemovie.db;

import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

//import static com.example.niswar.cataloguemovie.db.DatabaseContract.movieColumns.TABLE_MOVIE;

public class DatabaseContract {

    public static String TABLE_MOVIE = "table_movie";


    public static final class movieColumns implements BaseColumns{


        public static String ID = "_id";
        public static String MOVIE_ID = "movie_id";
        public static String TITLE = "title";
        public static String DESCRIPTION = "description";
        public static String RELEASE_DATE = "release_date";
        public static String POSTER_PATH = "poster_path";

    }

    public static final String AUTHORITY = "com.example.niswar.cataloguemovie";
    public static final Uri CONTENT_URI = new Uri.Builder().scheme("content")
            .authority(AUTHORITY)
            .appendPath(TABLE_MOVIE)
            .build();
    public static String getColumnString(Cursor cursor, String columnName) {
        return cursor.getString( cursor.getColumnIndex(columnName) );
    }
    public static int getColumnInt(Cursor cursor, String columnName) {
        return cursor.getInt( cursor.getColumnIndex(columnName) );
    }
    public static long getColumnLong(Cursor cursor, String columnName) {
        return cursor.getLong( cursor.getColumnIndex(columnName) );
    }

}
