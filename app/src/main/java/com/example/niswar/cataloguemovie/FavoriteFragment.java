package com.example.niswar.cataloguemovie;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import static com.example.niswar.cataloguemovie.db.DatabaseContract.CONTENT_URI;


public class FavoriteFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>{

    private RecyclerView rvMovie;
    private ListRecyclerMovieAdapter adapter;

    ProgressBar progressBar;
    TextView tvProgressBar;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment, container, false);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        CursorLoader cursorLoader = null;
        cursorLoader = new CursorLoader(getContext(),CONTENT_URI,null,null,null,null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        rvMovie.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        adapter.setMovieDataCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        rvMovie = (RecyclerView) view.findViewById(R.id.rv_movie_item);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvMovie.setLayoutManager(layoutManager);
        adapter = new ListRecyclerMovieAdapter(getContext());
        rvMovie.setAdapter(adapter);

        LoaderManager loaderManager = getLoaderManager();
        Loader<Cursor> loader = loaderManager.getLoader(0);


        Log.d("Test", "Debug onViewCreated()");

        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);   //Display Progress Bar

        if (loader == null) {
            loaderManager.initLoader(0, null, this);
        } else {
            loaderManager.restartLoader(0, null, this);
        }

    }

}








