package com.example.niswar.favoritemovie.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.niswar.favoritemovie.R;

import static com.example.niswar.favoritemovie.db.DatabaseContract.getColumnString;
import static com.example.niswar.favoritemovie.db.DatabaseContract.movieColumns.DESCRIPTION;
import static com.example.niswar.favoritemovie.db.DatabaseContract.movieColumns.RELEASE_DATE;
import static com.example.niswar.favoritemovie.db.DatabaseContract.movieColumns.TITLE;

public class FavoriteMovieAdapter extends CursorAdapter {
    public FavoriteMovieAdapter(Context context, Cursor c, boolean b) {
        super(context, c);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        if (cursor != null) {
            TextView tvTitle = (TextView) view.findViewById(R.id.tv_item_title);
            TextView tvDate = (TextView) view.findViewById(R.id.tv_item_release_date);
            TextView tvDescription = (TextView) view.findViewById(R.id.tv_item_description);

            tvTitle.setText(getColumnString(cursor, TITLE));
            tvDescription.setText(getColumnString(cursor, DESCRIPTION));
            tvDate.setText(getColumnString(cursor, RELEASE_DATE));

        }
    }
}
